<?php


interface IItem
{
    public function setCondition($condition);
    public function setPrice($price);
}