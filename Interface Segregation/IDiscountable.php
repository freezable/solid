<?php


interface IDiscountable
{
    public function applyDiscount($discount);
    public function applyPromocode($promocode);
}