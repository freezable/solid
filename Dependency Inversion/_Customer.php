<?php


class _Customer
{
    private $currentOrder = null;

    public function buyItems()
    {
        $processor = new _OrderProcessor();
        return $processor->checkout($this->currentOrder);
    }

}