<?php


class Customer
{
    private $currentOrder = null;

    public function buyItems(IOrderProcessorInterface $processor)
    {
        return $processor->checkout($this->currentOrder);
    }
}