<?php


interface IOrderProcessorInterface
{
    public function checkout($order);
}